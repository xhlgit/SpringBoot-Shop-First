package com.xhl.core.validate.sms;

import org.springframework.stereotype.Component;

/**
 * 默认的短信发送实现
 * @author xuhongliang
 */
@Component("smsCodeSender")
public class DefaultSmsCodeSender implements SmsCodeSender {

	/**
	 * 这里只是使用了控制台模拟了发送短信
	 * @param mobile 手机号码
	 * @param code 验证码
	 */
	@Override
	public void send(String mobile, String code) {
		System.out.println("向手机"+mobile+"发送短信验证码"+code);
	}

}
