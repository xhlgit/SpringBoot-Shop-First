package com.xhl.core.validate.sms;

import com.xhl.core.properties.base.SecurityProperties;
import com.xhl.core.validate.base.ValidateCode;
import com.xhl.core.validate.base.ValidateCodeGenerator;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码生成器，返回 ValidateCode
 * @author xuhongliang
 */
@Getter
@Setter
@Component("smsValidateCodeGenerator")
public class SmsCodeGenerator implements ValidateCodeGenerator {

	@Autowired
	private SecurityProperties securityProperties;

	@Override
	public ValidateCode generate(ServletWebRequest request) {
		//生成对应位数的随机数
		String code = RandomStringUtils.randomNumeric(
				securityProperties.getValidateCode().getSmsCode().getLength());
		return new ValidateCode(code,
				securityProperties.getValidateCode().getSmsCode().getExpireIn());
	}
}
