package com.xhl.core.validate.image;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.xhl.core.validate.base.impl.AbstractValidateCodeProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author 徐宏亮
 * @description 图片验证码处理器，返回图片给请求即可
 * @date 2019/3/4 22:42
 */
@Component("imageValidateCodeProcessor")
public class ImageCodeProcessor extends AbstractValidateCodeProcessor<ImageCode> {

	/**
	 * 发送图形验证码，将其写到响应中
	 * response.getOutputStream() 可实现将图片发送给请求
	 */
	@Override
	protected void send(ServletWebRequest request, ImageCode imageCode) throws Exception {
		if (request.getResponse() != null) {
			//将指定格式的任意 RenderedImage 写入 OutputStream
			ImageIO.write(imageCode.getImage(), "JPEG", request.getResponse().getOutputStream());
		}
	}

}
