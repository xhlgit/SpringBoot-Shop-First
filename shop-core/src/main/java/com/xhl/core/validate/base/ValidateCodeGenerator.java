package com.xhl.core.validate.base;

import com.xhl.core.validate.base.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author xuhongliang
 *
 */
public interface ValidateCodeGenerator {

	ValidateCode generate(ServletWebRequest request);
	
}
