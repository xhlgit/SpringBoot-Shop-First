package com.xhl.core.validate.base.impl;

import com.xhl.core.validate.base.ValidateCodeType;
import com.xhl.core.validate.base.exception.ValidateCodeException;
import com.xhl.core.validate.base.ValidateCode;
import com.xhl.core.validate.base.ValidateCodeGenerator;
import com.xhl.core.validate.base.ValidateCodeProcessor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.Map;


/**
 * 验证码处理器抽象类
 *
 * @author xuhongliang
 */
public abstract class AbstractValidateCodeProcessor<C extends ValidateCode> implements ValidateCodeProcessor {

    /**
     * 操作session的工具类
     */
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    /**
     * 依赖搜索
     * 收集系统中所有的 {@link ValidateCodeGenerator} 接口的实现。
     * Spring 看到这个 Map 的时候，会在启动时查找 Spring 容器里，所有这个接口的实现
     * 将找到的实现放到 Map 里，Bean 名作为 Mpa 的 Key
     */
    @Autowired
    private Map<String, ValidateCodeGenerator> validateCodeGenerators;

    /**
     * 创建验证码，并保存到 session ，并发送给用户
     * @param request
     * @throws Exception
     */
    @Override
    public void create(ServletWebRequest request) throws Exception {
        //生成
        C validateCode = generate(request);
        //保存
        save(request, validateCode);
        //发送
        send(request, validateCode);
    }

    /**
     * 生成校验码
     *
     * @param request
     * @return
     */
    @SuppressWarnings("unchecked")
    private C generate(ServletWebRequest request) {
        //获取校验码的请求类型
        String type = getValidateCodeType(request).toString().toLowerCase();
        //利用类型名称获取验证码生成器
        String generatorName = type + ValidateCodeGenerator.class.getSimpleName();
        ValidateCodeGenerator validateCodeGenerator = validateCodeGenerators.get(generatorName);
        if (validateCodeGenerator == null) {
            throw new ValidateCodeException("验证码生成器" + generatorName + "不存在");
        }
        //生成验证码
        return (C) validateCodeGenerator.generate(request);
    }

    /**
     * 保存校验码，存入 Session 中，后缀获取了当前实现类的前缀
     *
     * @param request
     * @param validateCode
     */
    private void save(ServletWebRequest request, C validateCode) {
        sessionStrategy.setAttribute(request, getSessionKey(request), validateCode);
    }

    /**
     * 构建验证码放入session时的key
     *
     * @param request ServletWebRequest
     * @return Key 值
     */
    private String getSessionKey(ServletWebRequest request) {
        return SESSION_KEY_PREFIX + getValidateCodeType(request).toString().toUpperCase();
    }

    /**
     * 发送校验码，由子类实现
     *
     * @param request      ServletWebRequest
     * @param validateCode ValidateCode 继承的或自己
     * @throws Exception
     */
    protected abstract void send(ServletWebRequest request, C validateCode) throws Exception;

    /**
     * 根据请求的url获取校验码的类型
     *
     * @param request 请求与相应的参数
     * @return ValidateCodeType
     */
    private ValidateCodeType getValidateCodeType(ServletWebRequest request) {
        //截取当前实现类的前缀，并代入枚举类获取最终配置的类型
        String type = StringUtils.substringBefore(getClass().getSimpleName(), "CodeProcessor");
        //返回指定名称 enum type 的枚举常量。
        return ValidateCodeType.valueOf(type.toUpperCase());
    }

    /**
     * 校验验证码
     * @param request 工具类，封装请求和响应
     */
    @SuppressWarnings("unchecked")
    @Override
    public void validate(ServletWebRequest request) {

        ValidateCodeType processorType = getValidateCodeType(request);
        String sessionKey = getSessionKey(request);

        //获取 session 中保存的验证码
        C codeInSession = (C) sessionStrategy.getAttribute(request, sessionKey);

        String codeInRequest;
        try {
            codeInRequest = ServletRequestUtils.getStringParameter(
                    request.getRequest(),
                    processorType.getParamNameOnValidate());
        } catch (ServletRequestBindingException e) {
            throw new ValidateCodeException("获取验证码的值失败");
        }

        if (StringUtils.isBlank(codeInRequest)) {
            throw new ValidateCodeException(processorType + "验证码的值不能为空");
        }

        if (codeInSession == null) {
            throw new ValidateCodeException(processorType + "验证码不存在");
        }

        if (codeInSession.isExpired()) {
            sessionStrategy.removeAttribute(request, sessionKey);
            throw new ValidateCodeException(processorType + "验证码已过期");
        }

        if (!StringUtils.equals(codeInSession.getCode().toLowerCase(), codeInRequest.toLowerCase())) {
            throw new ValidateCodeException(processorType + "验证码不匹配");
        }

        //用完后，删除 session 中保存的验证码
        sessionStrategy.removeAttribute(request, sessionKey);
    }

}
