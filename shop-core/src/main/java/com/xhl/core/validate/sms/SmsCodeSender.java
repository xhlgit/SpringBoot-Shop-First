package com.xhl.core.validate.sms;

/**
 * 短信发送接口
 * @author xuohongliang
 */
public interface SmsCodeSender {

	/**
	 * 短信发送方法
	 * @param mobile 手机号码
	 * @param code 验证码
	 */
	void send(String mobile, String code);

}
