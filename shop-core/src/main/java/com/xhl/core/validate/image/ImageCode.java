package com.xhl.core.validate.image;

import com.xhl.core.validate.base.ValidateCode;
import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * @author xuhongliang
 * @description 图形验证码类，继承验证码类
 * @date 2019/3/4 21:02
 */
@Getter
@Setter
public class ImageCode extends ValidateCode {
	/**
	 * 生成的图形验证码
	 */
	private BufferedImage image;

	/**
	 * 设置过期时间点，通过父类的 isExpired 判断是否过期
	 */
	public ImageCode(BufferedImage image, String code, int expireIn) {
		super(code, expireIn);
		this.image = image;
	}

	public ImageCode(BufferedImage image, String code, LocalDateTime expireTime) {
		super(code, expireTime);
		this.image = image;
	}
}
