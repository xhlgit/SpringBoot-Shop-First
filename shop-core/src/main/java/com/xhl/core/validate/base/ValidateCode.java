package com.xhl.core.validate.base;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * @author xuhongliang
 * @description 验证码类
 * @date 2019/3/4 21:02
 */
@Getter
@Setter
public class ValidateCode {
	/**
	 * 验证码
	 */
	private String code;

	/**
	 * 验证码对象创建时间
	 */
	private LocalDateTime expireTime;

	/**
	 * 指定有效期，当前时间加上输入的秒数为过期时间
	 * @param code 验证码
	 * @param expireIn 验证码有效秒数
	 */
	public ValidateCode(String code, int expireIn){
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
	}

	/**
	 * 指定过期时间
	 * @param code 验证码
	 * @param expireTime 验证码有效时间
	 */
	public ValidateCode(String code, LocalDateTime expireTime){
		this.code = code;
		this.expireTime = expireTime;
	}

	/**
	 * 验证是否超过有效期，判断当前时间是否超过过期时间
	 * @return true 超过 false 未超过
	 */
	public boolean isExpired() {
		return LocalDateTime.now().isAfter(expireTime);
	}
}
