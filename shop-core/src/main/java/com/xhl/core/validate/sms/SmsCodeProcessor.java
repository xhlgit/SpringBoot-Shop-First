package com.xhl.core.validate.sms;

import com.xhl.core.properties.SecurityConstantsProperties;
import com.xhl.core.validate.base.ValidateCode;
import com.xhl.core.validate.base.impl.AbstractValidateCodeProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 短信验证码处理器，生成环境因该在自己实现一个短信验证码发送器
 * 
 * @author xuhongliang
 *
 */
@Component("smsValidateCodeProcessor")
public class SmsCodeProcessor extends AbstractValidateCodeProcessor<ValidateCode> {

	/**
	 * 短信验证码发送器
	 */
	@Autowired
	private SmsCodeSender smsCodeSender;
	
	@Override
	protected void send(ServletWebRequest request, ValidateCode validateCode) throws Exception {
		//传递手机号的参数的名称
		String paramName = SecurityConstantsProperties.DEFAULT_PARAMETER_NAME_MOBILE;
		//利用参数获取请求传过来的手机号
		String mobile = ServletRequestUtils.getRequiredStringParameter(request.getRequest(), paramName);
		//传手机号和验证码给发送器，将验证码发送到对应的手机号
		smsCodeSender.send(mobile, validateCode.getCode());
	}

}
