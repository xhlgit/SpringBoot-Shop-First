package com.xhl.core.validate.base.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义异常类，这里没干什么，后面可自己加
 * @author xuhongliang
 */
public class ValidateCodeException extends AuthenticationException {

	private static final long serialVersionUID = -7285211528095468156L;

	public ValidateCodeException(String msg) {
		super(msg);
	}

}
