package com.xhl.core.social.qq.connet;

import com.xhl.core.social.qq.api.QQ;
import com.xhl.core.social.qq.api.QQImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * 负责1-5步的处理过程
 * @author xuhongliang
 * @date 2019/3/13
 */
public class QQServerProvider extends AbstractOAuth2ServiceProvider<QQ> {

    private String appId;

    /**
     * 用户导向服务器的url
     */
    private static final String URL_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";

    /**
     * 用户授权后申请令牌用的url
     */
    private static final String URL_ACCESS_TOKEN = "https://graph.qq.com/oauth2.0/token";

    public QQServerProvider(String appId, String appSecret) {
        /**
         *
         * @author xuhongliang
         * @date 2019/4/30
         * @param appId 客户端ID
         * @param appSecret 客户端秘钥
         * @param URL_AUTHORIZE 用户导向服务器的url
         * @param URL_ACCESS_TOKEN 用户授权后申请令牌用的url
         */
        super(new OAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_ACCESS_TOKEN));
    }

    /**
     * 每次都创建一个新的接口API，不能是单例的
     * @author xuhongliang
     * @date 2019/4/30
     * @param accessToken 令牌
     * @return com.xhl.core.social.qq.api.QQ
     */
    @Override
    public QQ getApi(String accessToken) {
        return new QQImpl(accessToken, appId);
    }
}
