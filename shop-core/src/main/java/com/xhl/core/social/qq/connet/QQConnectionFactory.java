package com.xhl.core.social.qq.connet;

import com.xhl.core.social.qq.api.QQ;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.OAuth2ServiceProvider;

/**
 * @author xuhongliang
 * @date 2019/4/30
 */
public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ> {

    public QQConnectionFactory(String providerId, String appId, String appSecret) {
        /**
         * Create a {@link OAuth2ConnectionFactory}.
         *
         * @param providerId      提供商的唯一标识 e.g. "facebook"
         * @param serviceProvider 对应实现的 ServiceProvider
         * @param apiAdapter      适配器
         */
        super(providerId, new QQServerProvider(appId, appSecret), new QQAdapter());
    }
}
