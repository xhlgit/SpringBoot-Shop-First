package com.xhl.core.social.qq.connet;

import com.xhl.core.social.qq.api.QQ;
import com.xhl.core.social.qq.api.QQUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author xuhongliang
 * @date 2019/4/30
 */
public class QQAdapter implements ApiAdapter<QQ> {

    /**
     * 测试API是否可用
     * @param api API类型
     */
    @Override
    public boolean test(QQ api) {
        return true;
    }

    /**
     * 适配 Connetion 和 API
     * @param api 获取用户信息接口
     * @param values 设置信息
     */
    @Override
    public void setConnectionValues(QQ api, ConnectionValues values) {
        QQUserInfo userInfo = api.getUserInfo();
        values.setDisplayName(userInfo.getNickname());//显示用户的名字
        values.setImageUrl(userInfo.getFigureurl_qq_1());//用户的头像
        values.setProfileUrl(null);//QQ没用，没有个人主页，不填
        values.setProviderUserId(userInfo.getOpenId());//服务商的用户id 也就是 openid
    }

    /**
     * 跟上面类似
     * @param api 获取用户信息接口
     * @return org.springframework.social.connect.UserProfile
     */
    @Override
    public UserProfile fetchUserProfile(QQ api) {
        return UserProfile.EMPTY;
    }

    /**
     * 和 setConnectionValues setProfileUrl 一样，QQ不能发消息更新
     * @author xuhongliang
     * @date 2019/4/30
     * @param api
     * @param message
     */
    @Override
    public void updateStatus(QQ api, String message) {
        //do noting
    }
}
