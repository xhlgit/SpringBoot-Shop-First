package com.xhl.core.social.qq.api;

import java.io.IOException;

/**
 * @author xuhongliang
 * @date 2019/3/13
 */
public interface QQ {
    /**
     * 获取QQ用户信息
     * @author xuhongliang
     * @date 2019/4/30
     * @param
     * @return com.xhl.core.social.qq.api.QQUserInfo
     */
    QQUserInfo getUserInfo();
}
