package com.xhl.core.properties;

/**
 * 登陆类型
 * @author xuhongliang
 */
public enum LoginResponseType {
	
	/**
	 * 跳转
	 */
	REDIRECT,
	
	/**
	 * 返回json
	 */
	JSON

}
