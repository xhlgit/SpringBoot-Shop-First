package com.xhl.core.properties.validate;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 22:03
 */
@Getter
@Setter
public class ImageCodeProperties extends SmsCodeProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    public ImageCodeProperties() {
        setLength(4);
    }
    private int width = 67;
    private int height = 23;
}
