package com.xhl.core.properties.browser;

import com.xhl.core.properties.LoginResponseType;
import com.xhl.core.properties.SecurityConstantsProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 19:57
 */
@Getter
@Setter
public class BrowserProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private String signUpUrl = "login";
    private int rememberMeSeconds = 3600;
    private String loginPage = SecurityConstantsProperties.DEFAULT_LOGIN_PAGE_URL;
    private LoginResponseType loginType = LoginResponseType.JSON;
}
