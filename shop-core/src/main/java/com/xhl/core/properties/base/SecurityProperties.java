package com.xhl.core.properties.base;

import com.xhl.core.properties.browser.BrowserProperties;
import com.xhl.core.properties.Social.SocialProperties;
import com.xhl.core.properties.validate.ValidateCodeProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 19:59
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "xhl.security")
@PropertySource("classpath:browser.properties")
public class SecurityProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private BrowserProperties browser = new BrowserProperties();
    private ValidateCodeProperties validateCode = new ValidateCodeProperties();
    private SocialProperties social = new SocialProperties();
}
