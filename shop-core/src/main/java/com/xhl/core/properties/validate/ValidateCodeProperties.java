package com.xhl.core.properties.validate;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 20:26
 */
@Getter
@Setter
public class ValidateCodeProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private ImageCodeProperties imageCode = new ImageCodeProperties();
    private SmsCodeProperties smsCode = new SmsCodeProperties();
}
