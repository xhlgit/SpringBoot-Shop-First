package com.xhl.core.properties.validate;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 19:57
 */
@Getter
@Setter
public class SmsCodeProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private int length = 4;
    private int expireIn = 60;
    private String url;
}
