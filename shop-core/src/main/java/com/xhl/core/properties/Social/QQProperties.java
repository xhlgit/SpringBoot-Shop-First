package com.xhl.core.properties.Social;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xuhongliang
 * @date 2019/4/30
 */
@Getter
@Setter
public class QQProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private String providerId = "qq";
}
