package com.xhl.core.properties.Social;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 20:27
 */
@Getter
@Setter
public class SocialProperties {
    // 如果配置了值，通过 SecurityProperties 获取的是配置文件里的值，没配就是这里的值
    private String appId;
    private String appSecret;
    private QQProperties qq = new QQProperties();
}
