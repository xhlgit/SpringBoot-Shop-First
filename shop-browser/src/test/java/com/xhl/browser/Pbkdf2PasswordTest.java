package com.xhl.browser;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Slf4j
public class Pbkdf2PasswordTest {

    @Value("${system.user.password.secret}")
    private String secret = null;

    @Test
    public void test() {
        String pwd = "password";
        Pbkdf2PasswordEncoder passwordEncoder = new Pbkdf2PasswordEncoder(secret);
        // 加密
        String encodedPassword = passwordEncoder.encode(pwd);
        log.info("【加密后的密码为：】" + encodedPassword);
    }
}
