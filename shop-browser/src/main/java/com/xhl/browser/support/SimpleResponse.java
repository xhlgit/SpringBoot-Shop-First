package com.xhl.browser.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/4 20:22
 */
@Getter
@Setter
@AllArgsConstructor
public class SimpleResponse {
    private Object content;
}
