package com.xhl.browser.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.log.StaticLog;
import com.xhl.browser.domain.SysUser;
import com.xhl.browser.domain.SysUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

/**
 * @author 徐宏亮
 * @description 获取用户输入的参数
 * @date 2018/12/24 09:39
 */
@Component
public class CustomUserServiceImpl implements UserDetailsService, SocialUserDetailsService {
    @Autowired
    public SysUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        // 按用户名或手机号查找
        SysUser user = userRepository.findByUserNameOrMobile(s, s);
        if (ObjectUtil.isNull(user)) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        StaticLog.info("s:"+s);
        StaticLog.info("表单登陆用户名:"+user.getUsername()+";表单登陆密码:"+user.getPassword());
        return user;
    }

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        SysUser user = userRepository.findByUserId(userId);
        if (ObjectUtil.isNull(user)) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        StaticLog.info("userId:"+userId);
        StaticLog.info("社交登陆用户名:"+user.getUsername()+";社交登陆密码:"+user.getPassword());
        return user;
    }
}
