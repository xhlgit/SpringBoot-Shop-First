package com.xhl.browser.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2018/12/24 09:39
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Msg {
    private String title;
    private String content;
    private String extraInfo;
}
