package com.xhl.browser.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author 徐宏亮
 * @description
 * @date 2018/12/29 14:12
 */

@Controller
@RequestMapping("/customer")
@ResponseBody
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/saveAll")
    public List<Customer> index() {
        List<Customer> customerArrayList = new ArrayList<>();
        customerArrayList.add(new Customer("Jack", "Bauer"));
        customerArrayList.add(new Customer("Chloe", "O'Brian"));
        customerArrayList.add(new Customer("Kim", "Bauer"));
        customerArrayList.add(new Customer("David", "Palmer"));
        customerArrayList.add(new Customer("Michelle", "Dessler"));
        customerArrayList.add(new Customer("Bauer", "Dessler"));
        List<Customer> customers = customerRepository.saveAll(customerArrayList);
        return customers;
    }

    @GetMapping("/customer")
    public List<Customer> findAll(){
        List<Customer> all = customerRepository.findAll();
        return all;
    }

    @GetMapping("/customer/{id}")
    public Optional<Customer> findById(@PathVariable("id") Long id){
        Optional<Customer> result = customerRepository.findById(id);
        return result;
    }

    @DeleteMapping("/customer/{id}")
    public void deleteById(@PathVariable("id") Long id){
        customerRepository.deleteById(id);
    }

    @GetMapping("/findByLastName/{lastName}")
    public List<Customer> findByLastName(@PathVariable("lastName") String lastName){
        List<Customer> result = customerRepository.findByLastName(lastName);
        return result;
    }

    @GetMapping("/findByLastName2/{lastName}")
    public List<Customer> findByLastName2(@PathVariable("lastName") String lastName){
        List<Customer> result = customerRepository.findByLastName2(lastName);
        return result;
    }

    @GetMapping("/findByName")
    public List<Customer> findByName(){
        //按照id倒序排列，直接创建sort对象，通过排序方法和属性名
//        Sort sort = new Sort(Sort.Direction.DESC,"id");
//        List<Customer> result = customerRepository.findByName("Bauer",sort);
//        return result;
        //按照id倒序排列，通过Sort.Order对象创建sort对象
//        Sort sort = new Sort(Sort.Direction.DESC,"id");
//        List<Customer> result = customerRepository.findByName("Bauer",sort);
//        return result;
        //按照id、firstName倒序排列，通过排序方法和属性List创建sort对象
//        List<String> sortProperties = new ArrayList<>();
//        sortProperties.add("id");
//        sortProperties.add("firstName");
//        Sort sort = new Sort(Sort.Direction.DESC,sortProperties);
//        List<Customer> result = customerRepository.findByName("Bauer",sort);
//        return result;
        //按照id、firstName倒序排列，通过创建Sort.Order对象的集合创建sort对象
        List<Sort.Order> orders = new ArrayList<>();
        orders.add(new Sort.Order(Sort.Direction.DESC,"id"));
        orders.add(new Sort.Order(Sort.Direction.ASC,"firstName"));
        List<Customer> result = customerRepository.findByName("Bauer",new Sort(orders));
        return result;
    }

    @GetMapping("/pageable")
    public Page<Customer> pageable(){
        Pageable pageable = PageRequest.of(3,3, Sort.Direction.DESC,"id");
        Page<Customer> page = customerRepository.findByName2("bauer",pageable);
        return page;
    }

    @GetMapping("/findAllProjections")
    public List<CustomerProjection> findAllProjections(){
        List<CustomerProjection> projections = customerRepository.findAllProjectedBy();
        for (CustomerProjection projection :
                projections) {
            System.out.println("FullName:" + projection.getFullName());
            System.out.println("FirstName:" + projection.getFirstName());
            System.out.println("LastName:" + projection.getLastName());
        }
        return projections;
    }

    @GetMapping("/QBE")
    public List<Customer> QBE(){
        //创建查询条件数据对象
        Customer customer = new Customer();
        customer.setFirstName("Jack");
        customer.setLastName("Xhl");

        //创建匹配器，即如何使用查询条件
        //构建对象
        ExampleMatcher matcher = ExampleMatcher.matching();
        //姓名采用“开始匹配”的方式查询
        matcher.withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith());
        //忽略属性：是否关注。因为是基本类型，需要忽略掉
        matcher.withIgnorePaths("focus");

        //创建实例
        Example<Customer> ex = Example.of(customer, matcher);

        //查询
        List<Customer> ls = customerRepository.findAll(ex);

        //返回数据
        return ls;
    }
}
