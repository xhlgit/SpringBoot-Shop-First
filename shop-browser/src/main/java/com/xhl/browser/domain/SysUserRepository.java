package com.xhl.browser.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @author 徐宏亮
 * @description SysUser数据访问
 * @date 2018/12/24 09:39
 */
@Repository
public interface SysUserRepository extends JpaRepository<SysUser, Long> {
    /**
     * 功能描述 按userName查询数据返回实体
     * @author 徐宏亮
     * @param userName 查询传入的参数
     * @return com.shop.domain.SysUser
     * @throws
     */
    SysUser findByUserName(String userName);

    SysUser findByUserId(String userId);

    /**
     * 功能描述 按mobile查询数据返回实体
     * @author 徐宏亮
     * @param mobile 查询传入的
     * @return com.shop.domain.SysUser参数
     * @throws
     */
    SysUser findByMobile(String mobile);

    SysUser findByUserNameOrMobile(String userName, String mobile);
}
