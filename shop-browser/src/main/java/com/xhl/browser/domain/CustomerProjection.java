package com.xhl.browser.domain;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author 徐宏亮
 * @description 有时候我们不希望输出全部的数据，就可以使用一个接口去当类型
 * @date 2019/1/4 14:25
 */
public interface CustomerProjection {
    @Value("#{target.firstName + ' ' + target.lastName}")
    String getFullName();

    String getFirstName();

    String getLastName();
}
