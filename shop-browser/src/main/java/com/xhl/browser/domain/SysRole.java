package com.xhl.browser.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author: 张彦奇
 * @date: 2018-11-06 20:10
 * @description: SysRole--角色类
 **/
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SysRole {
    @Id
    @GeneratedValue
    private Long roleId;
    private String roleName;

}
