package com.xhl.browser;

import com.xhl.core.properties.SecurityConstantsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import sun.security.util.SecurityConstants;

/**
 * 个性化用户认证流程
 * @author 徐宏亮
 * @description
 * @date 2019/3/6 21:54
 */
public class AbstractChannelSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    protected AuthenticationSuccessHandler shopAuthenticationSuccessHandler;

    @Autowired
    protected AuthenticationFailureHandler shopAuthenticationFailureHandler;

    protected void applyPasswordAuthenticationConfig(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage(SecurityConstantsProperties.DEFAULT_UNAUTHENTICATION_URL) //定义登陆页面
                .loginProcessingUrl(SecurityConstantsProperties.DEFAULT_LOGIN_PROCESSING_URL_FORM) //定义登陆成功页面
                .successHandler(shopAuthenticationSuccessHandler) //自定义登陆成功
                .failureHandler(shopAuthenticationFailureHandler);//自定义登陆失败
    }
}
