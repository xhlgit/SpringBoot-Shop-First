package com.xhl.demo.async;

import com.xhl.demo.someFilter.filter.TimeFilter;
import com.xhl.demo.someFilter.interceptor.TimeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * 添加第三方的拦截器类
 * Configuration 告诉 spring 这是一个配置类
 */
//@Configuration
public class AsyncWebConfig implements WebMvcConfigurer {

    /**
     * 功能描述 支持拦截异步的请求
     * @author 徐宏亮
     * @date 2019/3/2 13:29
     * @param configurer
     * @return void
     */
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        //configurer.registerCallableInterceptors();//对应方式 order2 拦截
        //configurer.registerDeferredResultInterceptors();//对应方式 order3 拦截
        //configurer.setDefaultTimeout(2000);//设置超时时间，对应时间没有返回就释放
        //configurer.setTaskExecutor();//设置一个线程池，取代 spring 默认的线程池
    }
}
