package com.xhl.demo.async;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author 徐宏亮
 * @description 模拟消息对象的对象
 * @date 2019/1/16 14:04
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class MockQueue {
    private String placeOrder;
    private String completeOrder;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 功能描述 这里模拟延迟一秒
     * @author 徐宏亮
     * @date 2019/3/2 13:12
     * @param placeOrder
     * @return void
     */
    public void setPlaceOrder(String placeOrder) {
        //需要多线程处理，不然会阻塞
        new Thread(() -> {
            logger.info("接到下单请求：" + placeOrder);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.completeOrder = placeOrder;
            logger.info("下单请求处理完毕：" + placeOrder);
        }).start();
    }

    public void setComplateOrder(String completeOrder) {
        this.completeOrder = completeOrder;
    }
}
