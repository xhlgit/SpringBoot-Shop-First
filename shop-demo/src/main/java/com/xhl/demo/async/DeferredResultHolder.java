package com.xhl.demo.async;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 徐宏亮
 * @description 传递对象用，放处理结果
 * @date 2019/1/16 14:10
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class DeferredResultHolder {
    private Map<String, DeferredResult<String>> map = new HashMap<>();
}
