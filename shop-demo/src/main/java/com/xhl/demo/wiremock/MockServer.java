package com.xhl.demo.wiremock;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.StrUtil;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/3/2 13:54
 */
public class MockServer {
    public static void main(String[] args) {
        configureFor(8062);
        removeAllMappings();
        mock("/order/1", "01");
        mock("/order/2", "02");
    }

    private static void mock(String url, String file) {
        ClassPathResource classPathResource = new ClassPathResource("/mock/response/" + file + ".txt");
        Object[] objects = FileUtil.readLines(classPathResource.getFile(), "UTF-8").toArray();
        String content = StrUtil.join("\n", objects);
        stubFor(get(urlPathEqualTo(url)).willReturn(aResponse().withBody(content).withStatus(200)));
    }
}
