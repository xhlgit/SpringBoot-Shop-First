package com.xhl.demo.controller.controllerException;

import com.xhl.demo.controller.controllerException.exception.UserNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 异常处理器，这里处理了拦截器就不会处理
 * ControllerAdvice 实现监控 Controller
 */
@ControllerAdvice
public class ControllerExceptionHandler {
    /**
     * ExceptionHandler 当控制器的任何方法发生这个异常时，就执行被注解的方法
     * ResponseBody 返回的信息转换为 json
     * ResponseStatus 定义返回的 http 状态码
     * @param ex 抛出的异常
     * @return 返回的就是请求者接收到的信息
     */
    @ExceptionHandler(UserNotExistException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, Object> handleUserNotExistException(UserNotExistException ex) {
        Map<String, Object> result = new HashMap<>();
        result.put("id", ex.getId());
        result.put("message", ex.getMessage());
        return result;
    }
}
