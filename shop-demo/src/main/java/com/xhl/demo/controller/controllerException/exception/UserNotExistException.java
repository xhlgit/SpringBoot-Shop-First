package com.xhl.demo.controller.controllerException.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNotExistException extends RuntimeException {

    private Long id;

    public UserNotExistException(Long id) {
        super("用户不存在");
        this.id = id;
    }
}
