package com.xhl.demo;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableSwagger2Doc
@SpringBootApplication(scanBasePackages={
        "com.xhl.browser",
        "com.xhl.core",
        "com.xhl.demo"})
@EnableJpaRepositories(basePackages = "com.xhl.browser.domain")
@EntityScan(basePackages = "com.xhl.browser.domain")
public class DemoSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoSpringBootApplication.class, args);
    }
}
