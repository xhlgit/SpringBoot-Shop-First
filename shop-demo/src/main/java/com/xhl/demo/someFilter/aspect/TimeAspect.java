package com.xhl.demo.someFilter.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;

import java.util.Date;

/**
 * @author 徐宏亮
 * @description 切片可以得到传入参数的值，但是拿不到原始的http的请求对象
 * Aspect 指定它是一个切片
 * Component 注入容器
 * 切入点（注解）
 *     1. 在那些方法上起作用 使用注解里的方法
 *     如 execution(* com.xhl.shop.shopsecuritydemo.controller.UserController.*(..))
 *     定义在那些方法生效
 *
 *     2. 在上面时候起作用 Before Around After AfterThrowing AfterReturning
 * @date 2019/1/15 21:03
 */
//@Aspect
//@Component
public class TimeAspect {

    // @Before() 方法执行之前
    // @After() 方法执行之后
    // @AfterThrowing 方法有异常
    // @AfterReturning 方法结束

    /**
     * 功能描述 环绕通知
     * 切入点可以用一些表达式，去官方文档学习怎么使用
     * @author 徐宏亮
     * @date 2019/1/15 21:12
     * @param pjp 包含拦截的方法的所有信息
     * @return java.lang.Object
     */
    @Around("execution(* com.xhl.shop.shopsecuritydemo.controller.UserController.*(..))")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("aspect time aspect start");
        // 获取控制器方法的参数数组
        Object[] args = pjp.getArgs();
        for (Object arg :
                args) {
            System.out.println("arg is " + arg);
        }
        long start = new Date().getTime();
        // 去调用被拦截的控制器方法
        Object proceed = pjp.proceed();
        System.out.println("aspect time filter 耗时:" + (new Date().getTime() - start));
        System.out.println("aspect time aspect end");
        return proceed;
    }

}
