package com.xhl.demo.someFilter.filter;

import com.xhl.demo.someFilter.interceptor.TimeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * 添加第三方的拦截器类
 * Configuration 告诉 spring 这是一个配置类
 */
//@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private TimeInterceptor timeInterceptor;

    /**
     * 功能描述 添加我们定义的拦截器
     * @author 徐宏亮
     * @date 2019/3/2 11:58
     * @param registry
     * @return void
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(timeInterceptor);
    }

    /**
     * 功能描述
     * @author 徐宏亮
     * @date 2019/3/2 11:48
     * @return org.springframework.boot.web.servlet.FilterRegistrationBean<com.xhl.demo.someFilter.filter.TimeFilter>
     */
    @Bean
    public FilterRegistrationBean<TimeFilter> timeFilter() {
        // 添加我们的或者第三的的过滤器，不需要 Component 注解，和 web.xml 配置 Filter 标签是一样的。
        FilterRegistrationBean<TimeFilter> filterRegistrationBean = new FilterRegistrationBean<>();

        // 将自己的过滤器加上
        TimeFilter timeFilter = new TimeFilter();
        filterRegistrationBean.setFilter(timeFilter);

        // 指定生效的 url
        List<String> urls = new ArrayList<>();
        urls.add("/*");
        filterRegistrationBean.setUrlPatterns(urls);

        return filterRegistrationBean;
    }
}
