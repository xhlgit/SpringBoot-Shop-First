package com.xhl.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/1/15 21:35
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo {
    private String path;
}
