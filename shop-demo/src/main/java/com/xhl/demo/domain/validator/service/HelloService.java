package com.xhl.demo.domain.validator.service;

import org.springframework.stereotype.Service;

@Service
public interface HelloService {
    String greeting(String name);
}
