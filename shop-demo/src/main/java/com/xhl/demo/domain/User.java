package com.xhl.demo.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.xhl.demo.domain.validator.MyConstraint;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.util.Date;

/**
 * @author 徐宏亮
 * @description
 * @date 2019/1/14 14:32
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    //用户简单视图
    public interface UserSimpleView {}
    //用户复杂视图
    public interface UserDetailView extends UserSimpleView {}

    // 使用 JsonView 注解，分配我们的属性给对应的接口，然后在 controller 匹配接口实现展示对应的属性

    @ApiModelProperty("用户id")
    @JsonView(UserDetailView.class)
    private Long id;
    @ApiModelProperty("用户姓名")
    @JsonView(UserSimpleView.class)
    @MyConstraint(message = "自定义验证测试，你填什么名称我都算你错！")
    private String username;
    @ApiModelProperty("用户密码")
    @NotBlank(message = "密码不能为空！")
    @JsonView(UserDetailView.class)
    private String password;
    @ApiModelProperty("用户生日")
    @Past(message = "生日不能是未来时间！")
    @JsonView(UserSimpleView.class)
    private Date birthday;
}
