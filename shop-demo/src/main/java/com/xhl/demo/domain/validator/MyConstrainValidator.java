package com.xhl.demo.domain.validator;

import com.xhl.demo.domain.validator.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * ConstraintValidator 第一个参数是指定实现的注解，第二个参数是能在那些字段类型上应用，这里Object就可以在所有类型的字段上
 * initialize 初始化做的一些工作
 * isValid 校验逻辑，第一个参数 传进来的校验值，第二个参数 校验的上下文
 */
public class MyConstrainValidator implements ConstraintValidator<MyConstraint, Object> {

    @Autowired
    HelloService helloService;

    @Override
    public void initialize(MyConstraint constraintAnnotation) {
        System.out.println("************* my validator init. *************");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String tom = helloService.greeting("tom");
        System.out.println("************* " + tom + " *************");
        System.out.println("************* " + value + "-校验的值 *****************");
        //false 验证失败，true 验证成功
        return false;
    }
}
