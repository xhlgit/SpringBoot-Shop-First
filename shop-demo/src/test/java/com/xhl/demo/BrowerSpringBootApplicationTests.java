package com.xhl.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrowerSpringBootApplicationTests {
    /**
     * 注入伪造的 mvc 环境
     */
    @Autowired
    private WebApplicationContext wac;

    /**
     * Before 注解的方法会在测试用例前执行
     * 构建伪造的 mvc 环境
     */
    private MockMvc mockMvc;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    /**
     * Test 表示测试用例
     * WithMockUser 自动登录用户，避免请求需要 spring security 验证
     * @Throws Exception perform 方法的异常
     */
    @Test
    @WithMockUser
    public void whenQuerySuccess() throws Exception {
        //mockMvc.perform() 发起一个模拟的请求，内部声明请求方法、参数名等参数
        String result = mockMvc.perform(get("/user")
                .param("id", "1")
                .param("username", "xhl")
                .param("password", "123")
                .param("size", "15")
                .param("page", "3")
                .param("sort", "userName,DESC")
                .contentType(MediaType.APPLICATION_JSON_UTF8))//定义编码
                // 希望服务器端返回的数据是什么样的
                .andExpect(status().isOk())//状态码要为200
                // 获取返回的json数据，这里意思是集合的长度为 3
                .andExpect(jsonPath("$.length()").value(3))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser
    public void whenQueryAllSuccess() throws Exception {
        String result = mockMvc.perform(get("/user/all")
                .param("id", "1")
                .param("username", "xhl")
                .param("password", "123")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser
    public void whenGenInfoSuccess() throws Exception {
        String result = mockMvc.perform(get("/user/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("tom"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser
    public void whenGetInfoFail() throws Exception {
        mockMvc.perform(get("/user/a")
            .contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().is4xxClientError());//正则只能匹配数字，这里返回400，我们才通过
    }

    @Test
    @WithMockUser
    public void whenCreateSuccess() throws Exception {
        Date date = new Date(
                LocalDateTime.now().minusYears(1)
                        .atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli());
        String content = "{\"username\":\"tom\",\"password\":null,\"birthday\":"+ date.getTime() +"}";
        String result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser
    public void whenUpdateSuccess() throws Exception {
        Date date = new Date(
                LocalDateTime.now().plusYears(1)
                        .atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli());
        String content = "{\"username\":\"tom\",\"password\":null,\"birthday\":"+ date.getTime() +"}";
        String result = mockMvc.perform(put("/user/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

    @Test
    @WithMockUser
    public void whenDeleteSucesses() throws Exception {
        mockMvc.perform(delete("/user/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void whenUploadSuccess() throws Exception {
        String result = mockMvc.perform(multipart("/file")
            .file(new MockMultipartFile(
                    "file",
                    "test.txt",
                    "multipart/form-data",
                    "hello upload".getBytes(StandardCharsets.UTF_8))))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }
}

