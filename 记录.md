## Restful API
### 使用 spring 测试框架测试用例
- @RestController 标明此 Controller 提供 RestAPI
- @RequestMapping 及其变体。映射 http 请求到 java 方法
- @RequestParam 映射请求参数到 java 方法的参数
- @PageableDefault 指定分页的默认值
- @PathVariable 映射 url 片段到 java 方法的参数
- url 中使用正则表达式
- @JsonView 控制 json 输出内容：
    - 在实体类，使用接口来声明多个视图
    - 在值对象的 get 方法上指定视图
    - 在 controller 方法上指定视图
- 测试用例可以保证，我们优化、重构、升级系统时，保证我们的系统没有问题
- @RequestBody 映射字符串请求体到 java 方法的参数
- 日期类型参数的处理
    - 前后端架构中，最好不要用带格式的日期
    - 使用不带格式的时间戳，然后在格式成别的形式
- 验证请求参数的合法性并处理校验结果
    - @Valid 在 controller 方法的参数上使用，配合实体类上的校验注解(Hibernate Validator)
    - 自定义消息 message 属性
    - 自定义校验注解 ConstraintValidator
    - BindingResult 处理校验结果，并且程序不会报错
- 405 请求的路径后台不支持

### Spring Boot 默认的错误处理机制
- 它会根据请求的终端来分别处理
- 使用 BasicErrorController 控制
- @RequestMapping(produces = "text/html") 判断请求头是否包含，如果包含证明你是游览器发起的请求，这样就可以分别处理了
- @RequestMapping @ResponseBody 不包含时返回 json ，我们使用 postman 就会得到 json
- 我们在 src/resources/resources/error/404.html 创建类似带请求吗的页面时，spring boot 遇到对应错误时会自动返回这个对应的状态码页面

### 自定义异常处理
- 可以继承异常类，定义自己的异常类，可直接排除
- ControllerAdvice 注解的类，可以监控控制器，在使用 ExceptionHandler 当控制器的任何方法发生这个异常时，就执行被注解的方法

### 拦截器处理
- Filter 过滤器
    - 继承 Filter 实现自己的过滤器监控所有的控制器，最后使用 Component 注解让它生效
    - 如果他是第三方用不了 Component 注解，可以用 Configuration 注解写一个配置类，然后利用 FilterRegistrationBean 添加过滤器，它相当于 web.xml 中的配置，还可以定义生效的 url 。
- Interceptor 拦截器，Spring 本身提供的类
    - 继承 HandlerInterceptor 重写方法实现自定义拦截器类
    - 使用注解 @Component 这样并不会失效不像 Filter
    - 继承 WebMvcConfigurer 使用 @Configuration 注解实现一个配置类（很多东西都可以塞在一个配置类里），重写 addInterceptors 方法，在里面添加我们自定义的拦截器类
    - Spring 的一些方法也会被拦截，需要注意
- Aspect 切面
    - Aspect 注解定义切片
    - 在那些方法上起作用 使用注解里的方法，如 execution(* com.xhl.shop.shopsecuritydemo.controller.UserController.*(..)) 定义在那些方法生效
    - 在上面时候起作用 Before Around After AfterThrowing AfterReturning 注解的方法

几个自定义拦截器的启动顺序：
1. Filter
2. Interceptor
3. ControllerAdvice
4. Aspect
5. Controller 执行

异常执行情况（如果有一个处理了，那么后面得就不会执行异常方法）：
1. Controller 执行有异常
2. Aspect 处理后在抛出
3. ControllerAdvice 处理后在抛出
4. Interceptor 处理后在抛出
5. Filter 处理后在抛出
6. tomcat 返回给用户

### 文件上传下载
略

### 异步请求
- 同步处理时，tomcat 的线程是有数量的
- 异步处理时，可以让主线程调用副线程处理请求，服务器的吞吐量有一定的提升

几种方式：
- 使用 Runnable 异步处理 Rest 服务，不能满足所有的场景，副线程必须是主线程掉起的
- 使用 DeferredResult 异步处理 Rest 服务

拦截异步请求的方式也不同，需要 WebMvcConfigurer configureAsyncSupport

### 与前端开发并行工作
- 使用 swagger 自动生成 html 文档
- 使用 WireMock 快速伪造 RESTful 服务

## Spring Security 开发基于表单的认证
### 过滤器链
系统启动时，spring boot 把所有过滤器配置好。
绿色过滤器用来验证用户的身份，每个过滤器处理一种过滤方式，检查请求是否为登陆请求。只要通过一个绿色过滤器即可登陆成功。

- 绿色 UsernamePasswordAuthenticationFilter 处理表单登陆
- 绿色 BasicAuthenticationFilter 处理http basic登陆
- 绿色 ...
- 蓝色 ExceptionTranslationFilter 根据异常引导用户
- 橙色 FilterSecurityInterceptor 决定当前请求能不能访问后面的服务，依据的是我们的身份认证配置

### 核心功能
- 身份认证
- 授权，你能干什么
- 攻击防护，防止伪造身份

没有任何配置时的默认实现功能：
- 保护了所以服务，必须进行身份认证，会进入一个默认的登陆页面
- 有一个初始用户，并且密码在启动过程中在控制台提供

自定义用户认证逻辑：
- 处理用户信息获取逻辑，封装在 UserDetailsService 接口上，重写它实现处理用户信息获取逻辑
- 处理用户校验逻辑 UserDetails
- 处理密码加密解密 PasswordEncoder

个性化用户认证流程：
- 自定义登陆页面 http.formLogin().loginPage("login.html")
- 自定义登陆成功处理 AuthenticationSuccessHandler
- 自定义登陆失败处理 AuthenticationFailureHandler

实现图形验证码：
- 开发生成图形验证码接口
- 根据随机数生成图片
- 随机数存在 Session 中
- 在将生成的图片写到接口的响应中
- 在认证流程中加入图形验证码校验

重构图形验证码：
- 验证码基本参数可配置
- 验证码拦截的接口可配置
- 验证码的生成逻辑可配置

图形验证码基本参数配置：
- 请求级配置 配置值在调用接口时传递
- 应用级配置 配置值写在 shop-demo
- 默认配置   配置值写在 shop-core

记住我
- 游览器发起认证请求
- UsernamePasswordAuthenticationFilter 认证成功
- 掉起 RememberMeService TokenRepository 将 Token 写入数据库
- 再将 Token 写入游览器 Cookie
- Cookie 记录后，不需要登录
- 请求会执行 RememberMeAuthenticationFilter 过滤器
- 过滤器读取 Cookie 中的 Token
- 在调用 RememberMeService 查找 Token
- 通过 Token 获取 UserDetails

记住我后
- 游览器发起认证请求
- RememberMeAuthenticationFilter 过滤器读取 Cookie 中的 Token
- 掉起 RemeberMeService TokenRepository 读取数据库中的 Token
- 验证有效性，有效不需要登录，无效需要登录
- 有效，将数据写入 UserDetailsService 对象

实现短信验证码登陆
- 开发短信验证码接口
- 校验短信验证码并登陆

core 配置代码结构
- 密码登陆的配置代码
- 短信登陆的配置代码
- 验证码相关的配置代码

替换 Spring security 逻辑
- UsernamePasswordAuthenticationFilter  ->  SmsAuthenticationFilter
- UsernamePasswordAuthenticationToken   ->  SmsAuthenticationToken(未认证)
- AuthenticationManager
- DaoAuthenticationProvider             ->  SmsAuthenticationProvider
- UserDetailsService
- UserDetails
- Authentication(已认证)                ->  SmsCodeAuthenticationSecurityConfig 认证的配置

## Spring Social 开发第三方认证
### OAuth 协议
意义：
- 微信给我一个令牌，不需要用户名密码
- 限制我访问的数据
- 令牌有一个有效期（一个月等）

角色：
- 服务提供商 Provider 提供令牌 Token
    - 认证服务器 Authorization Server 认证用户生成令牌
    - 资源服务器 Resource Server 数据及令牌
- 资源所有者 Resource Owner
- 第三方应用 Client

流程：

用户访问Client

1. Client 请求用户授权
2. 用户同意授权
3. Client 向认证服务器申请令牌
4. 认证服务器验证成功后发放令牌
5. Client 拿着令牌向资源服务器申请资源
6. 资源服务器验证成功后开发资源

授权模式：
- 授权码模式（最常用）
- 密码模式
- 客户端模式
- 简化模式

授权码模式流程：

用户访问Client

1. Client 将用户导向认证服务器
2. 用户同意授权
3. 认证服务器返回 Client 并携带授权码
4. Client 拿着授权码向认证服务器申请令牌
5. 认证服务器验证授权码成功后发放令牌
6. Client 获取用户信息

授权码模式的特点：
1. 用户验证实在认证服务器完成的
2. Client 必须有服务器

### Spring Social 基本原理
用户访问Client

1. Client 将用户导向认证服务器
2. 用户同意授权
3. 认证服务器返回 Client 并携带授权码
4. Client 拿着授权码向认证服务器申请令牌
5. 认证服务器验证授权码成功后发放令牌
6. Client 获取用户信息
7. 根据用户信息构建 Authentication 并放入 SecurityContext

Spring Social 将授权码模式流程封装到了 SocialAuthenticationFilter 加到 Spring security 过滤器链

**服务提供商的抽象类 AbstractOAuth2ServiceProvider**
- 实现自己的服务提供商类继承它即可
- 1-5步由 OAuth2Operations(OAuth2Template) 完成
- 第6步由 API(AbstractOAuth2ApiBinding) 完成，每个服务提供商都有区别，需要自己写自己的实现类完成信息的封装
- 第7步相关的接口和类
    - Connection(OAuth2Connection) 封装获取的用户信息
    - ConnectionFactory(OAuth2ConnectionFactory) 工厂，创建 Connection 实例
    - ServiceProvider 封装好放入 ConnectionFactory
    - ApiAdapter 将不同的 ServiceProvider 服务提供商统一封装好放入 ConnectionFactory
- 用户对应关系
    - 存在数据库的表里 UserConnection 表
    - UsersConnectionRepository(JdbcUserConnectionRepository) 针对 UserConnection 表做增删改查操作

### 类和接口的介绍
AbstractOAuth2ApiBinding
- 属性 String accessToken 存前5步获取的令牌，用户很多不能是单例
- 属性 RestTemplate restTemplate 用来发送 http 请求给服务商

QQ互联看API：
调用 OpenAPI 接口，网站需要将请你该去发送到某个具体的 OpenAPI 。
调用所有 OpenAPI ，除了各接口私有的参数外，所有 OpenAPI 都需要传入基于 OAuth2.0 协议的通用参数：
- access_token 可通过使用 Authorization_Code 获取 Access_Token 获取。有效期 3 个月。
- oauth_consumer_key 申请 QQ 登陆成功后，分配给应用的 appid
- openid 用户的 ID ，与 QQ 号码对应，可调用 `https://graph.qq.com/oauth2.0/me?access_token=YOUR_ACCESS_TOKEN` 获取

get_user_info 接口，需要上面三个参数
